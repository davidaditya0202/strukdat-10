/*
nama : David Aditya Susanto
NPM : 140810190067
Deskripsi : program binary search tree
tanggal : 10 mei 2020
*/
#include <iostream>
using namespace std;

struct tree{
		int value;
		tree* right;
		tree* left;
};

typedef tree *pointer;
typedef pointer Tree;

void createPohon(pointer &p_new);
void insertBST(tree &root,pointer p_new);
void preOrder(tree parent);
void postOrder(tree parent);
void inOrder(tree parent);

int main(){
	pointer p;
	tree akar;

	createPohon(p); //10
	insertBST(akar,p);

	createPohon(p); //8
	insertBST(akar,p);

	createPohon(p); //12
	insertBST(akar,p);

	createPohon(p); //3
	insertBST(akar,p);

	createPohon(p); //9
	insertBST(akar,p);

	createPohon(p); //2
	insertBST(akar,p);

	createPohon(p); //10
	insertBST(akar,p);

	createPohon(p); //12
	insertBST(akar,p);

	createPohon(p); //13
	insertBST(akar,p);

	preOrder(akar);
	inOrder(akar);
	postOrder(akar);

}

void createPohon(pointer &p_new){
		p_new = = new tree;
		cout << " masukan angka : ";
		cin >> p_new->value;
		p_new-> right = NULL;
		p_new-> left = NULL;
}

void insertBST(tree &root,pointer p_new){
		if(root = NULL){
			root = p_new;
		}
		bool lowerThanRoot = (p_new->value< root->value);

		if(lowerThanRoot){
			insertBST(root->right,p_new);
		}else{
			insertBST(root->left,p_new);
		}
}

void preOrder(tree parent){
		if (parent != NULL){
			cout << parent->value<<", ";
			preOrder(parent->left);
			preOrder(parent->right);
		}
}

void postOrder(tree parent){
		if(parent != NULL){
			postOrder(parent->left);
			postOrder(parent->right);
			cout << parent->value<<", ";
		}
}

void inOrder(tree parent){
		if(parent != NULL){
			inOrder(parent->left);
			cout << parent->value<<", ";
			inOrder(parent->right);
		}
}
